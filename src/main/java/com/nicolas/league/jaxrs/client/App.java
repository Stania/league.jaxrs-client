package com.nicolas.league.jaxrs.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import com.philippe.league.client.entity.League;
import com.philippe.league.client.entity.Player;
import com.philippe.league.client.entity.Team;

/**
 * Hello world!
 *
 */
public class App {

	static Client client = ClientBuilder.newClient();

	public static void main(String[] args) {

		League championsLeague = new League("Champions League", "football");
		Team psg = new Team("psg", "1ere division", championsLeague, "Paris");
		Player kurzawa = new Player("kurzawa", "prenom ", 6, psg, "actif",null,  250.0);

		League nfl = new League("NAF NAF LEAGUE", "football");
		League league1 = new League("league1", "soccer");
		League nba = new League("nba", "basketball");

		List<League> leagues = new ArrayList<League>(Arrays.asList(nfl, league1, nba));
		List<Team> teams = new ArrayList<Team>();
		List<Player> players = new ArrayList<Player>();

		int nbrEquipeParLeague;
		int nbrEquipes = 10;
		nbrEquipeParLeague = nbrEquipes / 3;
		int debut = 0;
		int fin = nbrEquipeParLeague;
		Random random = new Random();

		for (League league : leagues) {

			for (int i = debut; i < fin; i++) {
				Team team = new Team("team" + String.valueOf(i), "1ere division", league,
						"Oakland" + String.valueOf(i));

				teams.add(team);

				for (int j = 0; j < 10; j++) {
					String status = j % 2 == 0 ? "Actif" : "Inactif";
					Player player = new Player("player" + String.valueOf(j), "Mickael", random.nextInt(), team, status,
							new Date(random.ints(4).sum()), random.doubles(6).sum());
					players.add(player);
				}

			}
			Team team1 = new Team("team sans league", "League1", null, "Oakland");
			teams.add(team1);
			Player player1 = new Player("player", "Mickael", random.nextInt(), null, "Actif",
					new Date(random.ints(4).sum()), random.doubles(6).sum());
			players.add(player1);

			debut = fin;
			fin += nbrEquipeParLeague;
		}

		System.out.println(createPlayer(new Player("kurzawa", "prenom ", 6, null, "actif",null,  250.0)));
		System.out.println(createTeam(new Team("psg", "1 ere division ", , null, "actif",null,  250.0)));
		
	}

	public List<Player> createPlayers(List<Player> players) {
		List<Player> players2 = new ArrayList<Player>();
		for (Player player : players) {
			player = createPlayer(player);
			players2.add(player);
		}
		return players2;
	}

	public List<Team> createTeam(List<Team> teams) {
		return teams;
	}

	public List<League> createLeague(List<League> leagues) {
		return leagues;
	}

	private static Player createPlayer(Player player) {
		System.out.println(player);
		return client.target("http://10.0.0.53:8080/league/webapi/player/").queryParam("nom", player.getName())
				.queryParam("prenom", player.getFirstName()).queryParam("dossard", player.getJerseyNumber())
				.queryParam("statut", player.getStatus()).queryParam("salaire", player.getSalary())
				.request(MediaType.APPLICATION_JSON).get(Player.class);
	
	}
	private static Team createTeam(Team team) {
		System.out.println(team);
		return client.target("http://10.0.0.53:8080/league/webapi/team/").queryParam("nom", team.getName())
				.queryParam("division", team.getDivision()).queryParam("league", team.getLeague())
				.queryParam("city", team.getCity()).request(MediaType.APPLICATION_JSON).get(Team.class);
	
		
		//String name, String division, League league, String city
	}
}
