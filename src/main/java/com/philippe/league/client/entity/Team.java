package com.philippe.league.client.entity;

import java.util.Collection;

public class Team extends Identifier {

	/**
	 * 
	 */
	private static final long serialVersionUID = -728929211028398201L;

	private String division;
	
	private String city;
	
	

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	private League league;

	private Collection<Player> players;

	public Collection<Player> getPlayers() {
		return players;
	}

	public Team(String name, String division, League league, String city) {
		super(name);
		this.division = division;
		this.league = league;
		this.city = city;
	}
	
	

	public Team() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void setPlayers(Collection<Player> players) {
		this.players = players;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public League getLeague() {
		return league;
	}

	public void setLeague(League league) {
		this.league = league;
	}

	

	
	
	

}
