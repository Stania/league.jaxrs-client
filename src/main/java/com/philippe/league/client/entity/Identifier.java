package com.philippe.league.client.entity;

import java.io.Serializable;

public class Identifier  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;

	public Identifier() {
		
	}

	public Identifier(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Identifier [id=" + id + ", name=" + name + "]";
	}
	
	

}
